<!DOCTYPE html>
<html lang="en">

<head class="crypt-dark">
    <meta charset="UTF-8">
    <title>Moscow</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">	
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.toast.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/datatables-custom.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />
	<style>
		.error_input{
			background: #5c3f46 !important;
			color: #ffffff !important;
		}
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
<?php /*		div.dataTables_wrapper {
			width: 1000px;
			margin: 0 auto;
		} */ ?>
		
		div.dataTables_wrapper, #user_data {
			font-size:10px;
		}
<?php /*		#resize_wrapper {
			position: absolute;
			top: 5em;
			left: 1em;
			right: 1em;
			bottom: 1em;
		} 
		.big-col {
			width: 400px !important;
		} */ ?>
		.crypt-dark .crypt-header{
			box-shadow: none;
			background: #202943;
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			z-index: 9999;
			height: 50px;
			color: #fff;
		}
		.crypt-dark .crypt-heading-menu{
			list-style: none;
			padding-left: 0;
			margin: 8px 0;
			float: right;
		}
		body{
			background: #f9faff;
			overflow-x: hidden;
			margin-top: 40px;
			font-size: 11px;
		}
		iframe body{
			margin: 0;
		}
		.form-group{
			margin-bottom: 0.5rem;
		}
		.sm-gutters>.col, .sm-gutters>[class*=col-]{
			padding-right: 2px;
			padding-left: 2px;
		}
	</style>
</head>

<body class="crypt-dark" style="color:#fff">
    <header>
        <div class="container-full-width">
            <div class="crypt-header">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5">
                        
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 d-none d-md-block d-lg-block">
                        <ul class="crypt-heading-menu fright">
                            <li class="crypt-box-menu menu-green"><a href="<?php echo base_url().'main/logout'?>">logout</a></li>
                        </ul>
                    </div><i class="menu-toggle pe-7s-menu d-xs-block d-sm-block d-md-none d-sm-none"></i></div>
            </div>
        </div>
        <div class="crypt-mobile-menu">
            <ul class="crypt-heading-menu">
                <li class="crypt-box-menu menu-green"><a href="<?php echo base_url().'main/logout'?>">logout</a></li>
            </ul>
        </div>
    </header>
<br>    

<div class="container-fluid">
	<div class="row sm-gutters">
    <div class="col-xl-3">
        <div class="crypt-boxed-area">
<?php /*				<h6 class="crypt-bg-head mb-3">ORDER FORM</h6> */ ?>
			<div class="row no-gutters">
				<div class="col-6 pl-1 pr-1">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="Currency">Trader ID</label>
						</div>
						<div class="input-group">
							<?php if(count($trader_id) > 0):?>
								<select class="custom-select white" name="trader_id" id="trader_id">
								<?php foreach($trader_id as $each):?>
									<option value="<?php echo $each?>"><?php echo $each;?></option>
								<?php endforeach;?>
								</select>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
			<div class="row no-gutters">
				<div class="col pl-1 pr-1">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="Currency">EXCHANGE</label>
						</div>
						<div class="input-group">
							<?php if(count($all_table) > 0):?>
								<select class="custom-select white" name="exchange" id="exchange">
								<?php foreach($all_table as $each):?>
									<?php $crypto = substr($each,strlen(TABLE_PREFIX));?>
									<option value="<?php echo $crypto?>"><?php echo strtoupper($crypto);?></option>
								<?php endforeach;?>
								</select>
							<?php endif;?>
						</div>
					</div>
				</div>
				<div class="col pl-1 pr-1">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="SymbolList">SYMBOL</label>
						</div>
						<div class="input-group">
							<?php if(count($all_table) > 0):?>
								<select class="custom-select white" name="symbol" id="symbol">
									<option value="" disabled selected></option>
									<?php foreach($exchange as $each_exchange):?>
									<option value="<?php echo $each_exchange?>"><?php echo $each_exchange?></option>
									<?php endforeach;?>
								</select>
							<?php endif;?>
						</div>
					</div>
				</div>					
			</div>
			<div class="row no-gutters">
				<div class="col pl-1 pr-1">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="Side">SIDE</label>
						</div>
						<div class="input-group">
							<?php if(count($all_table) > 0):?>
								<select class="custom-select white" name="side" id="side">
									<option value="ALL">ALL</option>
									<option value="BUY">BUY</option>
									<option value="SELL">SELL</option>
								</select>
							<?php endif;?>
						</div>
					</div>
				</div>
				<div class="col pl-1 pr-1">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="qty">QUANTITY</label>
						</div>
						<input type="text" name="qty" id="qty" class="form-control" step="1" value="" min="0" />
					</div>
				</div>
			</div>
			<div class="row no-gutters">
				<div class="col-md-6">
					<div class="crypt-buy-sell-form">
						<p><b class="crypt-up">BUY</b></p>
						<div class="crypt-buy">
							<div class="form-group">
								<div class="d-flex justify-content-between">
									<label class="crypt-up" for="buy_limit">HIGH PRICE (H)</label>
								</div>
								<input type="number" name="high" id="high" class="form-control" step="0.5" value="" min="0" data-decimals="6" />
							</div>
							<div class="form-group">
								<div class="d-flex justify-content-between">
									<label class="crypt-up" for="low">LONG ENTRY PRICE</label>
								</div>
								<input type="number" name="buy_limit" id="buy_limit" class="form-control" step="0.5" value="" min="0" data-decimals="6" />
							</div>								
						</div>							
					</div>
				</div>
				<div class="col-md-6">
					<div class="crypt-buy-sell-form">
						<p><b class="crypt-down">SELL</b></p>
						<div class="crypt-sell">
							<div class="form-group">
								<div class="d-flex justify-content-between">
									<label class="crypt-down" for="sell_limit">LOW PRICE (L)</label>
								</div>
								<input type="number" name="low" id="low" class="form-control" step="0.5" value="" min="0" data-decimals="6" />
							</div>
							<div class="form-group">
								<div class="d-flex justify-content-between">
									<label class="crypt-down" for="high">SHORT ENTRY PRICE</label>
								</div>
								<input type="number" name="sell_limit" id="sell_limit" class="form-control" step="0.5" value="" min="0" data-decimals="6" />
							</div>
						</div>							
					</div>
				</div>
			</div>
			<div class="row no-gutters mb-3">
				<div class="col">
					<div class="d-flex justify-content-center">
						<div class="form-group col-md-7">
							<div class="d-flex justify-content-center">
								<label for="Spread">SPREAD</label>
							</div>
							<div class="input-group">
								<input type="number" name="spread" id="spread" class="form-control" step="0.1" value="0.5" min="0" max="100" data-decimals="2" data-suffix="%" />
								<div class="input-group-append">
									<span class="input-group-text">%</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
			
			<div class="row" id="ocodiv" style="display:none">
				<div class="col-6">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="Currency">OCO STOP USD</label>
							<div>
								<i class="fa fa-circle text-decoration-none crypt-up buysellindicator"></i>
								<i class="fa fa-circle text-decoration-none crypt-down buysellindicator"></i>
							</div>
						</div>
						<input type="text" name="oco_stop_usd" class="form-control">
					</div>
				</div>
			</div>
			<div class="row" id="tifdiv" style="display:none">
				<div class="col-6">
					<div class="form-group">
						<div class="d-flex justify-content-between">
							<label for="Currency">TIF DATE</label>
						</div>
						<input type="text" name="tif_date" class="form-control" id="datetimepicker" readonly="readonly"/>
					</div>
				</div>
			</div>
			<div class="row pb-6">
				<div class="col">
					<div class="d-flex justify-content-center">
						<div class="btn-group">
							<a href="" class="crypt-button-red-full" style="width:200px;" id="proceed">Proceed</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row pb-5 mt-3">
				<div class="col">
					<div class="d-flex justify-content-center" id="err_submit">
					</div>
				</div>
			</div>
				
        </div>
    </div>
	<div class="col-xl-9">
		<h6 class="crypt-bg-head mb-3">History Trades</h6>
		<?php if(count($columns)>0):?>
<?php /*			<div class="col-sm-12 p-0" style="position:static"> */ ?>
<?php /*				<div id="resize_wrapper"> */?>
<?php /*					<table id="user_data" class="dataTable display cell-border compact hover order-column row-border stripe"> */ ?>
					<table id="user_data" class="display" style="width:100%">
						<thead>
						<tr>
							<?php foreach($columns as $each_col): ?>
								<th class="big-col"><?php echo $each_col->alias ?></th>
							<?php endforeach;?>
						</tr>
						</thead>
					</table>
<?php /*				</div> */ ?>
<?php /*			</div> */ ?>
		<?php endif;?> 		
	</div>
	</div>
</div>
<footer>

</footer>
<script src="<?php echo base_url()?>assets/amc/core.js"></script>
<script src="<?php echo base_url()?>assets/amc/charts.js"></script>
<script src="<?php echo base_url()?>assets/amc/dark.js"></script>
<script src="<?php echo base_url()?>assets/amc/animated.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>

<script src="<?php echo base_url()?>assets/js/php-date-formatter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/js/php-date-formatter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.js"></script>
<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap-input-spinner.js"></script>
<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.bundle.js"></script>
<?php /* <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.js"></script> */ ?>
<script src="<?php echo base_url()?>assets/js/main.js"></script>
<script src="<?php echo base_url()?>assets/js/amc.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.toast.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.simple.websocket.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script>
    var uri = "<?php echo base_url()?>";
    var uri2 = '<?php echo "trading"?>' + '/';
    var ws = null;
	
<?php 
$columnDefs = array();
foreach($columns as $inc=>$each_col){
	$columndef = new stdClass();
	$columndef->targets = $each_col->targets;
	$columndef->width = $each_col->width;
	$columnDefs[] = $columndef;
}
?>
	columnDefs = <?php echo json_encode($columnDefs);?>;
	counter = <?php echo count($columnDefs);?>;
    $(document).ready(function(){
		$("input[id='high']").blur(function(){
			high_value = $('#high').val();
			spread_value = $('#spread').val();
			buy_limit = high_value * ( 1 + (spread_value/100));
			$('#buy_limit').val(buy_limit.toFixed(6));
			$('#buy_limit').removeClass('error_input');
		});
		
		$("input[id='low']").blur(function(){
			low_value = $('#low').val();
			spread_value = $('#spread').val();
			sell_limit = low_value * ( 1 - (spread_value/100));
			$('#sell_limit').val(sell_limit.toFixed(6));
			$('#sell_limit').removeClass('error_input');
		});

		$("input[id='spread']").blur(function(){
			high_value = $('#high').val();
			low_value = $('#low').val();
			spread_value = $('#spread').val();
			buy_limit = high_value * ( 1 + (spread_value/100));
			sell_limit = low_value * ( 1 - (spread_value/100));
			$('#buy_limit').val(buy_limit.toFixed(6));
			$('#sell_limit').val(sell_limit.toFixed(6));
			$('#buy_limit, #sell_limit').removeClass('error_input');
		});
		fetch_data();
        function fetch_data()
        {
            var dataTable = $('#user_data').removeAttr('width').DataTable({
				"paging": false,
				"searching": false,
                "processing" : true, 
                "serverSide" : true,
				"ordering": false,
                "order" : [],
                "ajax" : {
                    url: uri2 + "fetch",
                    type:"POST",
                },
<?php /*				"initComplete":function( settings, json){					
					$('#total_asset').html(json.total_asset);
				},*/ ?>
				"scrollX": true,
				"scrollY": "400px",
				"scrollCollapse": true,
				"paging": false,
<?php /*				"columnDefs": [
					{ width: 10, targets: 0 }
				],				
				"fixedColumns": true, */ ?>
				"scrollResize": true,
				"lengthChange": false,				
				"responsive": true,
				"bInfo" : false,
//				"columnDefs": columnDefs,
            });
/*			setInterval( function () {
				dataTable.ajax.reload();
			}, 30000 );	*/	
        }


    });

    $('input[name="low"], input[name="high"], input[name="spread"], input[name="sell_limit"], input[name="buy_limit"], input[name="qty"]').keyup(function(e){
        val = $(this).val();
		if(check_numeric(val) == false){
			$(this).addClass('error_input');
		}else{
			$(this).removeClass('error_input');
		}
    });
	
	function check_numeric(val){
		var reg = /^([0-9]|[1-9][0-9]+)(\.\d{0,6})?$/;
		var retval = false;
		if (reg.test(val)) {
			retval = true;
		}
		return retval;
	}
    var today = new Date();

    $('#datetimepicker').datetimepicker({
        minDate: new Date(),
        minTime: new Date(),
        disabledTimeIntervals: [
            [moment(), moment().hour(24).minutes(0).seconds(0)]
        ],
        format: 'Y-m-d H:i A',
        timepicker: true,
        onChangeDateTime: function(date) {
            var day = new Date(date).getDate();
            var monthIndex = new Date(date).getMonth();
            var year = new Date(date).getFullYear();

            if (day === today.getDate() && monthIndex == today.getMonth() && year == today.getFullYear()) {
                this.setOptions({minTime: new Date()});
            } else {
                this.setOptions({minTime: false});
            }
        }
    });

    function get_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit)
    {		
        return ({
            'requestId' : requestId.toString(),
            'traderId' : traderId,
            'exchange' : exchange,
            'symbol' : symbol,
            'qty' : qty,
            'low' : low,
            'high' : high,			
            'orderSide' : side,
			'spread' : spread,
			'sellLimit' : sell_limit,
			'buyLimit' : buy_limit
        });
    }
	
	function check_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit)
	{
		var status = true;
		var arrcheck = [];
		if(requestId == "" || requestId == null){
			arrcheck.push("RequestID must be filled");
			status = false;
		}
		if(traderId == "" || traderId == null){
			arrcheck.push("Trader ID must be filled");
			status = false;
		}
		if(exchange == "" || exchange == null){
			arrcheck.push("Exchange must be filled");
			status = false;
		}
		if(symbol == "" || symbol == null){
			arrcheck.push("Symbol must be filled");
			status = false;
		}
		if(side == "" || side == null){
			arrcheck.push("Side must be filled");
			status = false;
		}
		if(!check_numeric(qty)){
			arrcheck.push("Quantity must be a number");
			status = false;
		}
		if(!check_numeric(high)){
			arrcheck.push("High Price (H) must be a number");
			status = false;
		}
		if(!check_numeric(low)){
			arrcheck.push("Low Price (L) must be a number");
			status = false;
		}
		if(!check_numeric(buy_limit)){
			arrcheck.push("Long Entry Price must be a number");
			status = false;
		}
		if(!check_numeric(sell_limit)){
			arrcheck.push("Short Entry Price must be a number");
			status = false;
		}
		if(!check_numeric(spread)){
			arrcheck.push("Spread must be a number");
			status = false;
		}
		return JSON.stringify({"status":status,"arrcheck":arrcheck});
	}
	
	function isJSON(params) {
		if (typeof params != 'string')
			params = JSON.stringify(params);

		try {
			JSON.parse(params);
			return true;
		} catch (e) {
			return false;
		}
	}
	
	function human_readable_error(params){
		var response;

		switch(params) {
		  case "":
			response = "Something happening";
			break;
		  default:
			response = system_human_readable_error(params);
		}
		var side = $('#side').val();
		return response;
	}
	
	function system_human_readable_error(params){
		params = params.replace("-"," ");
		params = params.replace("_"," ");
		params = params.charAt(0).toUpperCase() + params.slice(1).toLowerCase();
		return params;
	}
	
	function showSubmitTrade(message, index){
		if(message.hasOwnProperty('resultMsg')){
			result = JSON.stringify(message);
			resultobj = JSON.parse(result);
			if(resultobj.resultMsg == 'received' || resultobj.resultMsg == 'done'){
				$.toast({
//								heading: 'Success',
					icon: 'success',
					text: human_readable_error(resultobj.resultMsg),
					showHideTransition: 'fade',
					position: 'bottom-center',
					stack: false,
					loader: false
				});
			}else{
				$.toast({
//								heading: 'Error',
					icon: 'error',
//								text: resultobj.resultMsg,
					text: human_readable_error(resultobj.resultMsg),
					showHideTransition: 'fade',
					position: 'bottom-center',
					stack: false,
					loader: false
				})
			}
			return false;
		};		
	}

    $('#proceed').click(function(){
		$('#err_submit').html('');
		var requestId = Date.now();
        traderId = $('#trader_id').val();
        exchange = $('#exchange').val();
        symbol = $('#symbol').val();
        qty = $('#qty').val();
        low = $('#low').val();
        high = $('#high').val();
        side = $('#side').val();
		spread = $('#spread').val();
		sell_limit = $('#sell_limit').val();
		buy_limit = $('#buy_limit').val();
		
		error_side_message = '';
		switch(side.toLowerCase()){
			case 'buy':
				error_side_message = "<b>Buy limit order could not place</b>: <br>";
			break;
			case 'sell':
				error_side_message = "<b>Sell limit order could not place</b>: <br>";
			break;
			case 'all':
				error_side_message = "<b>Limit order could not place</b>: <br>";
			break;			
			default:
		}
		
		var validation_checker = check_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit);
		validation_checker_data = JSON.parse(validation_checker);
		validation_checker_data_status = validation_checker_data.status;
		validation_checker_data_arrcheck = validation_checker_data.arrcheck;
		
		if(validation_checker_data_status === false){
			var li_html = '';
			var toast_err = '<ul>';
			$.each(validation_checker_data_arrcheck, function( index, value ) {
				li_html += '<li>'+value+'</li>';
			});
			$.each(validation_checker_data_arrcheck, function( index, value ) {
				toast_err += '<li>'+value+'</li>';
			});
			toast_err += '</ul>';

			toast_err = error_side_message + toast_err;

			rethtml = 
			'<div class="order-errors error_input pr-1">'+
				'<div class="d-flex justify-content-end">'+
					'<i class="fa fa-warning order-errors__icon"></i>'+
				'</div>'+
				'<ul class="order-errors__wrapper">'+li_html+'</ul>'+
			'</div>';
//			$('#err_submit').html(rethtml);
                $.toast({
//                    heading: 'Error',
                    text: toast_err,
                    showHideTransition: 'fade',
                    icon: 'error',
                    position: 'bottom-right',
                    stack: false,
                    loader: false
                })

			return false;
		}
		
		var data = get_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit);
		data = JSON.stringify(data);

        $.ajax({
            url: uri + "trading/submit_trade",
            method:"POST",
            data: data,
            success:function(response)
            {
				console.log(response);
				if(!isJSON(response)){
					$.toast({
//						heading: 'Error',
						text: error_side_message + human_readable_error("Something happened"),
						showHideTransition: 'fade',
						icon: 'error',
						position: 'bottom-right',
						stack: false,
						loader: false
					});					
					return false;
				}
				res = JSON.parse(response);
//				console.log(res);				
				res.forEach(showSubmitTrade);
            },
            error:function(err)
            {
                $.toast({
//                    heading: 'Error',
                    text: error_side_message + human_readable_error(err.responseText),
                    showHideTransition: 'fade',
                    icon: 'error',
                    position: 'bottom-right',
                    stack: false,
                    loader: false
                })
            }
        });
		return false;
    });
</script>
</body>
</html>