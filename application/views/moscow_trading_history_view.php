<!DOCTYPE html>
<html lang="en">

<head class="crypt-dark">
    <meta charset="UTF-8">
    <title>Moscow</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
<?php /*    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type='text/css'> */ ?>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css" type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.datetimepicker.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery.toast.css">
</head>

<body class="crypt-dark">
    <header>
        <div class="container-full-width">
            <div class="crypt-header">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5">
                        
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 d-none d-md-block d-lg-block">
                        <ul class="crypt-heading-menu fright">
							<li><a href="<?php echo base_url().'trading_history'?>">trading history</a></li>
                            <li class="crypt-box-menu menu-green"><a href="<?php echo base_url().'main/logout'?>">logout</a></li>
                        </ul>
                    </div><i class="menu-toggle pe-7s-menu d-xs-block d-sm-block d-md-none d-sm-none"></i></div>
            </div>
        </div>
        <div class="crypt-mobile-menu">
            <ul class="crypt-heading-menu">
                <li class="crypt-box-menu menu-green"><a href="<?php echo base_url().'main/logout'?>">logout</a></li>
            </ul>
        </div>
    </header>
<br>    

<div class="container-fluid">
	<div class="row sm-gutters">
	<div class="col-12">
		<div class="contents-custom">
			<div class="crypt-market-status">
				<div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Time</th>
                                                <th scope="col">Symbol</th>
                                                <th scope="col">Exchange</th>
                                                <th scope="col">Side 1</th>
                                                <th scope="col">Price 1</th>
                                                <th scope="col">Qty 1</th>
                                                <th scope="col">Status 1</th>
                                                <th scope="col">Order ID 1</th>
                                                <th scope="col">Order Type1</th>
                                                <th scope="col">Side 2</th>
                                                <th scope="col">Price 2 TP</th>
                                                <th scope="col">Qty 2</th>
                                                <th scope="col">Qty 2 Filled</th>
                                                <th scope="col">Status 2</th>
                                                <th scope="col">Order ID 2</th>
                                                <th scope="col">Order Type 2</th>
                                                <th scope="col">Profit</th>
                                                <th scope="col">High</th>
                                                <th scope="col">Low</th>
                                                <th scope="col">Qty</th>
                                                <th scope="col">Last Top Bottom Price</th>
                                                <th scope="col">Cut Lost ID</th>
                                                <th scope="col">Cut Lost Price</th>
                                                <th scope="col">Pair ID</th>
                                                <th scope="col">Price 2 Current CL</th>
                                                <th scope="col">Price 2 CL History</th>
                                                <th scope="col">Order ID 2 History</th>
                                                <th scope="col">Qty 2 Filled</th>
                                            </tr>
                                        </thead>
										<tbody>
											<?php foreach($trades as $each_trades):?>
												<tr>
													<th><?php echo $each_trades->create_time?></th>
													<td class="crypt-up"><?php echo $each_trades->symbol?></td>
													<td class="crypt-up"><?php echo $each_trades->exchange?></td>
													<td class="crypt-up"><?php echo $each_trades->side1?></td>
													<td class="crypt-up"><?php echo $each_trades->price1?></td>
													<td class="crypt-up"><?php echo $each_trades->qty1?></td>
													<td class="crypt-up"><?php echo $each_trades->status1?></td>
													<td class="crypt-up"><?php echo $each_trades->order_id1?></td>
													<td class="crypt-up"><?php echo $each_trades->order_type1?></td>
													<td class="crypt-up"><?php echo $each_trades->side2?></td>
													<td class="crypt-up"><?php echo $each_trades->price2_tp?></td>
													<td class="crypt-up"><?php echo $each_trades->qty2?></td>
													<td class="crypt-up"><?php echo $each_trades->qty2_filled?></td>
													<td class="crypt-up"><?php echo $each_trades->status2?></td>
													<td class="crypt-up"><?php echo $each_trades->order_id2?></td>
													<td class="crypt-up"><?php echo $each_trades->order_type2?></td>
													<td class="crypt-up"><?php echo $each_trades->profit?></td>
													<td class="crypt-up"><?php echo $each_trades->high?></td>
													<td class="crypt-up"><?php echo $each_trades->low?></td>
													<td class="crypt-up"><?php echo $each_trades->qty?></td>
													<td class="crypt-up"><?php echo $each_trades->last_top_bottom_price?></td>
													<td class="crypt-up"><?php echo $each_trades->cutloss_id?></td>
													<td class="crypt-up"><?php echo $each_trades->cutloss_price?></td>
													<td class="crypt-up"><?php echo $each_trades->pair_id?></td>
													<td class="crypt-up"><?php echo $each_trades->price2_current_cl?></td>
													<td class="crypt-up"><?php echo $each_trades->price2_cl_history?></td>
													<td class="crypt-up"><?php echo $each_trades->order_id2_history?></td>
													<td class="crypt-up"><?php echo $each_trades->qty2_filled?></td>
												</tr>
											<?php endforeach;?>
										</tbody>
<?php /*                                        <tbody>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-up">Buy</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-down">Sell</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-up">Buy</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-down">Sell</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-up">Buy</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-down">Sell</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-up">Buy</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-down">Sell</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.000056</td>
                                                <td class="crypt-down">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                            <tr>
                                                <th>22:35:59</th>
                                                <td class="crypt-up">Buy</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.000056</td>
                                                <td class="crypt-up">0.0003456</td>
                                                <td>5.3424984</td>
                                            </tr>
                                        </tbody> */ ?>
                                    </table>
					
				</div>
			</div>			
		</div>
	</div>
	</div>
</div>
<footer>

</footer>
<script src="<?php echo base_url()?>assets/amc/core.js"></script>
<script src="<?php echo base_url()?>assets/amc/charts.js"></script>
<script src="<?php echo base_url()?>assets/amc/dark.js"></script>
<script src="<?php echo base_url()?>assets/amc/animated.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>

<script src="<?php echo base_url()?>assets/js/php-date-formatter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url()?>assets/js/php-date-formatter.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.datetimepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/moment.js"></script>
<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap-input-spinner.js"></script>
<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.bundle.js"></script>
<?php /* <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.js"></script> */ ?>
<script src="<?php echo base_url()?>assets/js/main.js"></script>
<script src="<?php echo base_url()?>assets/js/amc.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.toast.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.simple.websocket.min.js"></script>
<?php /* <script src="https://s3.tradingview.com/tv.js"></script> */ ?>
<script>
//    $("input[type='number']").inputSpinner();
</script>
<script>
    var uri = "<?php echo base_url()?>";
    var ws = null;
<?php /*	
	function connect() {
//		var ws = new WebSocket('ws://10.48.1.76:8282');
//		var ws = new WebSocket('ws://localhost:8080/chat');
		var ws = new WebSocket('ws://121.7.235.126:8080/chat');
		ws.onopen = function() {
		};

		ws.onclose = function(e) {
			ws = connect();
		};

		ws.onmessage = function(e) {
			message = JSON.parse(e.data);
			if(message.hasOwnProperty('resultMsg')){
				console.log(message);
				result = JSON.stringify(message);
				resultobj = JSON.parse(result);
				if(resultobj.resultMsg == 'received' || resultobj.resultMsg == 'done'){
					$.toast({
						heading: 'Success',
						icon: 'success',
						text: resultobj.resultMsg,
						showHideTransition: 'fade',
						position: 'bottom-center',
						stack: false,
						loader: false
					});
				}else{
					$.toast({
						heading: 'Error',
						icon: 'error',
						text: resultobj.resultMsg,
						showHideTransition: 'fade',
						position: 'bottom-center',
						stack: false,
						loader: false
					})
				}
				return false;
			};
		};

		ws.onerror = function(err) {
			console.error('Socket encountered error: ', err.message, 'Closing socket');
			ws.close();
		};
		return ws;
		
	}
*/ ?>	

    $(document).ready(function(){
<?php /*		ws = connect(); */ ?>
		$("input[id='high']").blur(function(){
			high_value = $('#high').val();
			spread_value = $('#spread').val();
			buy_limit = high_value * ( 1 + (spread_value/100));
			$('#buy_limit').val(buy_limit.toFixed(6));
			$('#buy_limit').removeClass('error_input');
		});
		
		$("input[id='low']").blur(function(){
			low_value = $('#low').val();
			spread_value = $('#spread').val();
			sell_limit = low_value * ( 1 - (spread_value/100));
			$('#sell_limit').val(sell_limit.toFixed(6));
			$('#sell_limit').removeClass('error_input');
		});

		$("input[id='spread']").blur(function(){
			high_value = $('#high').val();
			low_value = $('#low').val();
			spread_value = $('#spread').val();
			buy_limit = high_value * ( 1 + (spread_value/100));
			sell_limit = low_value * ( 1 - (spread_value/100));
			$('#buy_limit').val(buy_limit.toFixed(6));
			$('#sell_limit').val(sell_limit.toFixed(6));
			$('#buy_limit, #sell_limit').removeClass('error_input');
		});

    });
	


    $('input[name="low"], input[name="high"], input[name="spread"], input[name="sell_limit"], input[name="buy_limit"], input[name="qty"]').keyup(function(e){
        val = $(this).val();
		if(check_numeric(val) == false){
			$(this).addClass('error_input');
		}else{
			$(this).removeClass('error_input');
		}
//        if($.isNumeric(val) == false){
//            $(this).addClass('error_input');
//        }else{
//            $(this).removeClass('error_input');
//        }
    });
	
	function check_numeric(val){
		var reg = /^([0-9]|[1-9][0-9]+)(\.\d{0,6})?$/;
		var retval = false;
		if (reg.test(val)) {
			retval = true;
		}
		return retval;
	}

/*    $('input[name="qty"]').keyup(function(e){
        val = $(this).val();
        if($.isNumeric(val) == false){
            $(this).addClass('error_input');
        }else{
			if(val < 0){
				$(this).addClass('error_input');
			}else{
				$(this).removeClass('error_input');
			}
        }
    });*/

    var today = new Date();

    $('#datetimepicker').datetimepicker({
        minDate: new Date(),
        minTime: new Date(),
        disabledTimeIntervals: [
            [moment(), moment().hour(24).minutes(0).seconds(0)]
        ],
        format: 'Y-m-d H:i A',
        timepicker: true,
        onChangeDateTime: function(date) {
            var day = new Date(date).getDate();
            var monthIndex = new Date(date).getMonth();
            var year = new Date(date).getFullYear();

            if (day === today.getDate() && monthIndex == today.getMonth() && year == today.getFullYear()) {
                this.setOptions({minTime: new Date()});
            } else {
                this.setOptions({minTime: false});
            }
        }
    });

    function get_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit)
    {		
        return ({
            'requestId' : requestId.toString(),
            'traderId' : traderId,
            'exchange' : exchange,
            'symbol' : symbol,
            'qty' : qty,
            'low' : low,
            'high' : high,			
            'orderSide' : side,
			'spread' : spread,
			'sellLimit' : sell_limit,
			'buyLimit' : buy_limit
        });
    }
	
	function check_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit)
	{
		var status = true;
		var arrcheck = [];
		if(requestId == "" || requestId == null){
			arrcheck.push("RequestID must be filled");
			status = false;
		}
		if(traderId == "" || traderId == null){
			arrcheck.push("Trader ID must be filled");
			status = false;
		}
		if(exchange == "" || exchange == null){
			arrcheck.push("Exchange must be filled");
			status = false;
		}
		if(symbol == "" || symbol == null){
			arrcheck.push("Symbol must be filled");
			status = false;
		}
		if(side == "" || side == null){
			arrcheck.push("Side must be filled");
			status = false;
		}
		if(!check_numeric(qty)){
			arrcheck.push("Quantity must be a number");
			status = false;
		}
		if(!check_numeric(high)){
			arrcheck.push("High Price (H) must be a number");
			status = false;
		}
		if(!check_numeric(low)){
			arrcheck.push("High Price (H) must be a number");
			status = false;
		}
		if(!check_numeric(buy_limit)){
			arrcheck.push("Long Entry Price must be a number");
			status = false;
		}
		if(!check_numeric(sell_limit)){
			arrcheck.push("Short Entry Price must be a number");
			status = false;
		}
		if(!check_numeric(spread)){
			arrcheck.push("Spread must be a number");
			status = false;
		}
		return JSON.stringify({"status":status,"arrcheck":arrcheck});
	}

    $('#proceed').click(function(){
		$('#err_submit').html('');
		var requestId = Date.now();
        traderId = $('#trader_id').val();
        exchange = $('#exchange').val();
        symbol = $('#symbol').val();
        qty = $('#qty').val();
        low = $('#low').val();
        high = $('#high').val();
        side = $('#side').val();
		spread = $('#spread').val();
		sell_limit = $('#sell_limit').val();
		buy_limit = $('#buy_limit').val();
		
		var validation_checker = check_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit);
		
		validation_checker_data = JSON.parse(validation_checker);
		validation_checker_data_status = validation_checker_data.status;
		validation_checker_data_arrcheck = validation_checker_data.arrcheck;
		
		if(validation_checker_data_status === false){
			var li_html = '';
			$.each(validation_checker_data_arrcheck, function( index, value ) {
				li_html += '<li>'+value+'</li>';
			});
			
			rethtml = 
			'<div class="order-errors error_input pr-3">'+
				'<div class="d-flex justify-content-end">'+
					'<i class="fa fa-warning order-errors__icon"></i>'+
				'</div>'+
				'<ul class="order-errors__wrapper">'+li_html+'</ul>'+
			'</div>';
			$('#err_submit').html(rethtml);
			return false;
		}
		
		var data = get_init_value(requestId,traderId,exchange,symbol,qty,low,high,side,spread,sell_limit,buy_limit);
		data = JSON.stringify(data);

        $.ajax({
            url: uri + "trading/submit_trade",
            method:"POST",
            data: data,
            success:function(response)
            {
				console.log(response);
				res = JSON.parse(response);
				$.each(res, function(i) {
					message = res[i];
					if(message.hasOwnProperty('resultMsg')){
						result = JSON.stringify(message);
						resultobj = JSON.parse(result);
						if(resultobj.resultMsg == 'received' || resultobj.resultMsg == 'done'){
							$.toast({
								heading: 'Success',
								icon: 'success',
								text: resultobj.resultMsg,
								showHideTransition: 'fade',
								position: 'bottom-center',
								stack: false,
								loader: false
							});
						}else{
							$.toast({
								heading: 'Error',
								icon: 'error',
								text: resultobj.resultMsg,
								showHideTransition: 'fade',
								position: 'bottom-center',
								stack: false,
								loader: false
							})
						}
						return false;
					};
				});				
            },
            error:function(err)
            {
                $.toast({
                    heading: 'Error',
                    text: err.responseText,
                    showHideTransition: 'fade',
                    icon: 'error',
                    position: 'bottom-right',
                    stack: false,
                    loader: false
                })
            }
        });
		return false;
<?php /*		if(ws.readyState == WebSocket.OPEN){
			ws.send(JSON.stringify(data));
		}
		return false;*/ ?>
    });
</script>
</body>
</html>