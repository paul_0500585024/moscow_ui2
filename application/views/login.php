<!DOCTYPE html>
<html lang="en">

<head class="crypt-dark">
    <meta charset="UTF-8">
    <title>Moscow</title>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/icons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.css">
	<style>
	input[type='text'], input[type='password'] {color:#fefefe}
	</style>
</head>

<body class="crypt-dark">
<?php /*    <header>
        <div class="container-full-width">
            <div class="crypt-header">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="crypt-logo"><img src="images/logo.jpg" alt=""></div>
                            </div>
                            <div class="col-xs-2">
                                <div class="crypt-mega-dropdown-menu"> <a href="" class="crypt-mega-dropdown-toggle">BTC/ETH <i class="pe-7s-angle-down-circle"></i></a>
                                    <div class="crypt-mega-dropdown-menu-block">
                                        <div class="crypt-market-status">
                                            <div>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td class="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td class="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">LTC</th>
                                                                    <td>0.0000564</td>
                                                                    <td>6.6768876</td>
                                                                    <td>-6.7%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">ETH</th>
                                                                    <td>0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-down">-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td class="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td class="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td class="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">LTC</th>
                                                                    <td>0.0000564</td>
                                                                    <td>6.6768876</td>
                                                                    <td>-6.7%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td class="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td class="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Coin</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Volume</th>
                                                                    <th scope="col">Change</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">BTC</th>
                                                                    <td class="crypt-down">0.000056</td>
                                                                    <td>5.3424984</td>
                                                                    <td class="crypt-down"><b>-5.4%</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td class="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">DOGE</th>
                                                                    <td class="crypt-up">0.0000234</td>
                                                                    <td>4.3456600</td>
                                                                    <td class="crypt-up">-9.6%</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">XMR</th>
                                                                    <td>0.0000567</td>
                                                                    <td>4.3456600</td>
                                                                    <td>-5.6%</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 d-none d-md-block d-lg-block">
                        <ul class="crypt-heading-menu fright">
                            <li><a href="exchange.html">Exchange</a></li>
                            <li><a href="market-overview.html">Overview</a></li>
                            <li><a href="marketcap.html">Market Cap</a></li>
                            <li><a href="trading.html">Trading</a></li>
                            <li><a href="withdrawl.html">Account</a></li>
                            <li class="crypt-box-menu menu-red"><a href="register.html">register</a></li>
                            <li class="crypt-box-menu menu-green"><a href="login.html">login</a></li>
                        </ul>
                    </div><i class="menu-toggle pe-7s-menu d-xs-block d-sm-block d-md-none d-sm-none"></i></div>
            </div>
        </div>
        <div class="crypt-mobile-menu">
            <ul class="crypt-heading-menu">
                <li class="active"><a href="">Exchange</a></li>
                <li><a href="">Market Cap</a></li>
                <li><a href="">Treanding</a></li>
                <li><a href="">Tools</a></li>
                <li class="crypt-box-menu menu-red"><a href="">register</a></li>
                <li class="crypt-box-menu menu-green"><a href="">login</a></li>
            </ul>
            <div class="crypt-gross-market-cap">
                <h5>$34.795.90 <span class="crypt-up pl-2">+3.435 %</span></h5>
                <h6>0.7925.90 BTC <span class="crypt-down pl-2">+7.435 %</span></h6></div>
        </div>
    </header> */ ?>
    
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="cryptorio-forms cryptorio-forms-dark text-center pt-5 pb-5">
<?php /*                    <div class="logo">
                        <img src="images/logo.png" alt="logo-image">
                    </div> */ ?>
                    <h3 class="p-4">Welcome To Login</h3>
					<?php if(isset($error)) { echo $error; }; ?>
                    <div class="cryptorio-main-form">
                        <form method="POST" action="<?php echo base_url() ?>main" class="text-left">
							<div class="form-group">
								<label for="email">Username</label>
								<input type="text" id="username" name="username" placeholder="Your username">
								<?php echo form_error('username'); ?>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" id="password" name="password" placeholder="Please Input Your Password">
								<?php echo form_error('password'); ?>
							</div>

                            <input type="submit" value="Log In" class="crypt-button-red-full">
                        </form>
<?php /*                        <p class="float-left"><a href="register.html">Sign Up</a></p>
                        <p class="float-right"><a href="forgot.html">Forgot Password</a></p> */ ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

	<script src="<?php echo base_url()?>assets/amc/core.js"></script>
	<script src="<?php echo base_url()?>assets/amc/charts.js"></script>
	<script src="<?php echo base_url()?>assets/amc/dark.js"></script>
	<script src="<?php echo base_url()?>assets/amc/animated.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url()?>assets/js/popper.min.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.js"></script>
	<script src="<?php echo base_url()?>assets/js/main.js"></script>
	<script src="<?php echo base_url()?>assets/js/amc.js"></script>
</body>
</html>