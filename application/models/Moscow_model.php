<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moscow_model extends CI_Model
{

    public function __construct()
    {
		//load helper moscow.php
        $this->load->helper('moscow');
    }

    public function show_databases(){
		
		//show database query
        if (($query = $this->db->query('SHOW DATABASES')) === FALSE)
        {
            return FALSE;
        }
        return $query;

    }

    public function get_all_table(){
		
		//get all table from database crypto_moscow
        $tables = $this->db->list_tables();
        $spesifix_tables = array();
        foreach($tables as $each){
            if(startsWith($each,TABLE_PREFIX)){
                $spesifix_tables[] = $each;
            }
        }
        return $spesifix_tables;
    }
	
	public function get_symbol(){
		
		//get query from table symbol
		$this->db->select('symbol');
		$this->db->from('symbol');
		return $this->db->get();
	}

	public function get_user_login($username = '', $password = ''){
		
		//get query from table user and check if username and password exists in tables
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->from('user');
		return $this->db->get();
	}
	
	public function get_traderid($username = ''){
		
		//get query from table user from specific username 
		$this->db->where('username', $username);
		$this->db->from('user');
		return $this->db->get();
	}
	
	public function get_trades(){
		
		//get data from trades
		$this->db->select('symbol, exchange, side1, ROUND(price1,4) AS price1, ROUND(qty1,4) AS qty1 , status1, order_id1, side2, order_id2, ROUND(price2_tp,4) AS price2_tp, ROUND(qty2,4) AS qty2, ROUND(qty2_filled,4) AS qty2_filled, status2, ROUND(profit,4) AS profit, high, low, qty, price2_current_cl, price2_cl_history, order_id2_history, ROUND(last_top_bottom_price,4) AS last_top_bottom_price, create_time as create_time, order_type1, order_type2, cutloss_id, cutloss_price, pair_id');		
		$this->db->from('trades');
		$this->db->order_by('id', 'DESC');
		return $this->db->get();
	}

	public function get_trades_daily($date = '' ){
		
		//get data from trades by daily
		if($date == '') $date = date('Y-m-d');
		$this->db->from('trades');
		$this->db->where('DATE(create_time)', $date);
		$this->db->order_by('id', 'DESC');
		return $this->db->get();
	}
	
	public function get_latest_trades($date){
		
		//get data from trades with greater than specific dates
		$this->db->from('trades');
		$this->db->where('create_time > ',$date);
		$this->db->order_by('create_time', 'DESC');
		return $this->db->get();
	}

	public function get_latest_trades_id($id){
		
		//get data from trades with greater than specific id
		$this->db->from('trades');
		$this->db->where('id > ',$id);
		$this->db->order_by('id', 'DESC');
		return $this->db->get();
	}
	
}