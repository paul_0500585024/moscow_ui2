<?php
require_once APPPATH."third_party/Pawl/vendor/autoload.php";

defined('BASEPATH') OR exit('No direct script access allowed');

class Trading extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		
		//initial set data_trader_encoded to empty string
		$this->data_trader_encoded = '';
        
		if ($this->session->userdata['logged_in'] == FALSE){
			//if session is not there, redirect to login page
            redirect(base_url().'main'); 
        }
		
		//initial value
		$this->message_result = array();
        $this->error = false;
		
		//load moscow model
        $this->load->model('Moscow_model');
		
		//load websocket Ratchet reactjs https://github.com/ratchetphp/Ratchet
		$loop = React\EventLoop\Factory::create();
		$connector = new Ratchet\Client\Connector($loop);
		$app = function (Ratchet\Client\WebSocket $conn) use ($connector, $loop, &$app) {			
			$conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn, $connector, $loop, &$app) {

				//if get message from server web socket
				$this->message_result[] = json_decode($msg);
				
				//close the connection after 0.5seconds to continue other events in one session
				$loop->addTimer(0.5, function () use ($conn, $connector, $loop, $app) {
					$conn->close();
				});
			});
			$conn->on('close', function ($code = null, $reason = null) use ($connector, $loop, $app) {
		
				//if the websocket close, print get result from websocket server
				echo json_encode($this->message_result);				
			});
			
			//send data from client data to webserver
			$conn->send($this->data_trader_encoded);
		};
		
		//make websocket result to be known in a class
		$this->loop = $loop;
		$this->connector = $connector;
		$this->app = $app;		
        $this->initialize();		
    }
	
    private function initialize()
    {
		//initialize value
		
		//initial gen_coluns
		$gen_columns = array();
				
		//initial list_columns
		$list_columns = array(
			array( "alias"=>"Time", "name" => "create_time", "width" => "100px", "targets" => 0  ),
			array( "alias"=>"Symbol", "name" => "symbol", "width" => "20px", "targets" => 1 ),
			array( "alias"=>"Exchange", "name" => "exchange", "width" => "20px", "targets" => 2 ),
			array( "alias"=>"Side 1", "name" => "side1", "width" => "20px", "targets" => 3 ),
			array( "alias"=>"Price 1", "name" => "price1", "width" => "20px", "targets" => 4 ),
			array( "alias"=>"Qty 1", "name" => "qty1", "width" => "20px", "targets" => 5 ),
			array( "alias"=>"Status 1", "name" => "status1", "width" => "20px", "targets" => 6 ),
			array( "alias"=>"Status 2", "name" => "status2", "width" => "20px", "targets" => 7 ),
			array( "alias"=>"Price 2 TP", "name" => "price2_tp", "width" => "20px", "targets" => 8 ),
			array( "alias"=>"Price 2 Current CL", "name" => "price2_current_cl", "width" => "20px", "targets" => 9 ),
			array( "alias"=>"Order ID 1", "name" => "order_id1", "width" => "20px", "targets" => 10 ),
			array( "alias"=>"Order Type 1", "name" => "order_type1", "width" => "20px", "targets" => 11 ),
			array( "alias"=>"Side 2", "name" => "side2", "width" => "20px", "targets" => 12 ),
			array( "alias"=>"Qty 2", "name" => "qty2", "width" => "20px", "targets" => 13 ),
			array( "alias"=>"Qty 2 Filled", "name" => "qty2_filled", "width" => "20px", "targets" => 14 ),
			array( "alias"=>"Order ID 2", "name" => "order_id2", "width" => "20px", "targets" => 15 ),
			array( "alias"=>"Order Type 2", "name" => "order_type2", "width" => "20px", "targets" => 16 ),
			array( "alias"=>"Profit", "name" => "profit", "width" => "20px", "targets" => 17 ),
			array( "alias"=>"High", "name" => "high", "width" => "20px", "targets" => 18 ),
			array( "alias"=>"Low", "name" => "low", "width" => "20px", "targets" => 19 ),
			array( "alias"=>"Qty", "name" => "qty", "width" => "20px", "targets" => 20 ),
			array( "alias"=>"Last Top Bottom Price", "name" => "last_top_bottom_price", "width" => "20px", "targets" => 21 ),
			array( "alias"=>"Price 2 CL History", "name" => "price2_cl_history", "width" => "20px", "targets" => 22 ),
			array( "alias"=>"Cutloss Price", "name" => "cutloss_price", "width" => "20px", "targets" => 23 ),
			array( "alias"=>"Order ID 2 History", "name" => "order_id2_history", "width" => "20px", "targets" => 24 ),
			array( "alias"=>"Cutloss ID", "name" => "cutloss_id", "width" => "20px", "targets" => 25 ),
			array( "alias"=>"Pair ID", "name" => "pair_id", "width" => "20px", "targets" => 26 ),
		);
		
		//set gen columns value name,alias,targets,width
		foreach($list_columns as $key=>$each){
			$column = new stdClass();
			$column->name = $each['name'];
			$column->alias = $each['alias'];			
			$column->targets = $each['targets'];			
			$column->width = $each['width'];			
			$gen_columns[$each['targets']] = $column;
		}
		
		//columns name
		$this->columns = $gen_columns;
    }

    public function index()
	{
		//set data_all_table (only accept binance connection)
        $data['all_table'] = array('symbol_binance'); 
		
		//get query from moscow model.php (function get_symbol)
		$query = $this->Moscow_model->get_symbol(); 
		
		//initial exchange
		$exchange = array();
		foreach($query->result() as $each){
			$exchange[] = $each->symbol;
		}		
		
		//set data_exchange
		$data['exchange'] = $exchange;
		
		//set user_name from session user_name set to trader name
		$user_name = $this->session->userdata["user_name"];
		
		//get query from moscow_model.php (function get_traderid)
		$query_trader_id = $this->Moscow_model->get_traderid($user_name); 

		//initial trader_id
		$trader_id = array();

		//set trader_id
		foreach($query_trader_id->result() as $each){
			$trader_id[] = $each->trader_id;
		}
		
		//set data_trader_id
		$data['trader_id'] = $trader_id;
		
		//set columns
		$data['columns'] = $this->columns;
		
		//load view
		$this->load->view('moscow_view', $data);
	}
	
	
	public function fetch()
	{
		//print to datatable format
		
		//get params from datatable draw
        $params['draw'] = $this->input->post('draw');
		
		//get params from datatable columns
        $params['columns'] = $this->input->post('columns');

		//get params from datatable start
        $params['start'] = $this->input->post('start');

		//get params from datatable length
        $params['length'] = $this->input->post('length');

		//get params from datatable start
        $params['search'] = $this->input->post('search');
		
		//get params from datatable order
        $params['order'] = $this->input->post('order');		
		
		//get data traders from model moscow.php (get_trades)
		$raw_data = $this->Moscow_model->get_trades();
		
		//initial data
		$data = array();
		
		//put data traders from model moscow.php to data
		foreach ($raw_data->result() as $key_raw_data=>$each) {
            $sub_array = array();
			foreach ($this->columns as $key_columns=>$each_col) {
				$col = $each_col->name;
				$width = $each_col->width;
				$sub_array[] = '<div class="update" data-column="' . $col . '" >' . $each->$col . '</div>';				
			}
			$data[] = $sub_array;			
		}

		//set record total from model moscow.php
		$record_total = count($raw_data->result());		

		//set output
        $output = array(
            "draw"    => intval($_POST["draw"]),
            "recordsTotal"  =>  $record_total,
            "recordsFiltered" => $record_total,
            "data"    => $data
        );

		//print the output to json format
        echo json_encode($output);
	}

	private function check_exists($data, $column_name, $column_alias){
		
		//check is data_column_name set
        if (!isset($data->$column_name)) {
			
			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");

			//print error message
            echo "$column_alias is not exists<br>";

			//return function
            return;
        }
    }

    private function check_not_empty($data, $column_name, $column_alias){

		//check is data_column_name is empty
        if ($data->$column_name == '') {
			
			//set error true
            $this->error = true;

			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");

			//print error message
            echo "$column_alias is empty<br>";

			//return function
            return;
        }
    }
    private function is_decimal($data, $column_name, $column_alias)
    {
		//check is data_column_name is decimal
        if(!preg_match('/^[0-9]{1,6}(\.[0-9]{1,5})?$/', $data->$column_name)){

			//set error true
            $this->error = true;
			
			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");

			//print error message
            echo "$column_alias is not a number<br>";

			//return function
            return;
        }
    }


    public function check_length($data, $column_name, $column_alias, $length){
		//check if length data_column should be lower than length given
        if(strlen($data->$column_name) > $length){
			//set error true
            $this->error = true;
			
			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");
			
			//print error message
            echo "$column_alias is over than $length<br>";

			//return function
            return;
        }
    }
	
	private function connectToServer($connector, $loop, $app)
	{
		//connection to Hai websocket
		$connector('ws://10.48.1.76:8282')
			->then($app, function (\Exception $e) use ($loop) {
				
				//print could not connect to Hai websocket
				echo "Could not connect: {$e->getMessage()}\n";
				
				//stop websocket connection
				$loop->stop();
			});
	}	
	

	public function submit_trade()
    {
		//get raw data
        $data_raw = file_get_contents("php://input");
		
        if($data_raw == ''){
			//if raw data empty
            
			//set error
			$this->error = true;
			
			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");
            
			//print "error. no data"
			echo "Error. No Data";
			
			//return to funtion
            return;
        }
		
        if(json_decode($data_raw) === NULL){
			//get data_raw is null
			
			//set error
            $this->error = true;
			
			//set http header internal server error
            header("HTTP/1.0 500 Internal Server Error");

			//print "error. data invalid"
            echo "Error. Data Invalid";
			
			//return to funtion
            return;
        }
		
		//decode data_raw
        $data = json_decode($data_raw);
		
        if($this->error){
			//if data decode error
			
			//return
           return;
        } 

		//set requestId from time and random number
        $requestId = time().rand(0,1000);

		//set data_trader from variable input by trader
        $data_trader = array(
            "requestId" => $requestId,
            "traderId" => $data->traderId,
            "exchange" => $data->exchange,
            "symbol" => $data->symbol,
            "qty" => $data->qty,
            "low" => $data->low,
            "high" => $data->high,
            "orderSide" => $data->orderSide,
			"spread" => $data->spread,
			"sellLimit" => $data->sellLimit,
			"buyLimit" => $data->buyLimit,
        );

		//encode data trader
        $this->data_trader_encoded = json_encode($data_trader);
		
		echo $this->data_trader_encoded;exit();
		
		//send data trader to websocket Hai
		$this->connectToServer($this->connector, $this->loop, $this->app);
		
		//websocket loop run
		$this->loop->run();
    }
		
}