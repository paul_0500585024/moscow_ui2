<?php
require_once APPPATH."third_party/Pawl/vendor/autoload.php";

defined('BASEPATH') OR exit('No direct script access allowed');

class Trading_history extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata['logged_in'] == FALSE){
            redirect(base_url().'main'); //if session is not there, redirect to login page
        }
        $this->load->model('Moscow_model');		
    }

    public function index()
	{
		$query = $this->Moscow_model->get_trades();
		$trades = array();
		foreach($query->result() as $each){
			$trades_data = new stdClass();
			$trades_data->symbol = $each->symbol;
			$trades_data->exchange = $each->exchange;
			$trades_data->side1 = $each->side1;
			$trades_data->price1 = $each->price1;
			$trades_data->qty1 = $each->qty1;
			$trades_data->status1 = $each->status1;
			$trades_data->order_id1 = $each->order_id1;
			$trades_data->side2 = $each->side2;
			$trades_data->order_id2 = $each->order_id2;
			$trades_data->price2_tp = $each->price2_tp;
			$trades_data->qty2 = $each->qty2;
			$trades_data->qty2_filled = $each->qty2_filled;
			$trades_data->status2 = $each->status2;
			$trades_data->profit = $each->profit;
			$trades_data->high = $each->high;
			$trades_data->low = $each->low;
			$trades_data->qty = $each->qty;
			$trades_data->price2_current_cl = $each->price2_current_cl;
			$trades_data->price2_cl_history = $each->price2_cl_history;
			$trades_data->order_id2_history = $each->order_id2_history;
			$trades_data->last_top_bottom_price = $each->last_top_bottom_price;
			$date = DateTime::createFromFormat('Y-m-d H:i:s', $each->create_time);
			$trades_data->create_time = $date->format('H:i:s');
			$trades_data->order_type1 = $each->order_type1;
			$trades_data->order_type2 = $each->order_type2;
			$trades_data->cutloss_id = $each->cutloss_id;
			$trades_data->cutloss_price = $each->cutloss_price;
			$trades_data->pair_id = $each->pair_id;
			$trades[] = $trades_data;
		}
		$data['trades'] = $trades;
		$this->load->view('moscow_trading_history_view', $data);
	}

	

}
