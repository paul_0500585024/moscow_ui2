<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file'); //load helpers folder, file.php (System)
        $this->load->library('form_validation'); //load library folder, form_validation.php (System)
		$this->load->model('Moscow_model');	//load model folder, moscow_model.php (Application)
    }

    public function index()
    {
        if($this->session->userdata('logged_in') == "true"){ 
			//if already has session, go to dashboard
            redirect(base_url()."trading");
        }else{ 
			//if not has session

            //set form validation
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            //set message form validation
            $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
	                <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

            //check validation
            if ($this->form_validation->run() == TRUE) {

                //get data
                $username = $this->input->post("username", TRUE);
                $password = md5($this->input->post('password', TRUE));

                //checking data from moscow model
                $checking = FALSE;
				$query = $this->Moscow_model->get_user_login($username, $password);
		
				//set query result to variable
                foreach($query->result() as $each){
					$checking = array(
						'user_id' => $each->id,
						'user_name' => $each->username,
						'user_pass' => $each->password,
					);
                }

                //if user login is true, create session
                if ($checking != FALSE) {
						//set session data
                        $session_data = array(
                            'user_id'   => $checking['user_id'],
                            'user_name' => $checking['user_name'],
                            'user_pass' => $checking['user_pass'],
                            'logged_in' => TRUE
                        );
                        //set session userdata
                        $this->session->set_userdata($session_data);

						//redirect to controller trading
                        redirect(base_url().'trading'); 

                }else{

                    $data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
	                	<div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> username or password is wrong!</div></div>';
                    $this->load->view('login', $data); //load view login.php and parsing variable data
                }

            }else{
				
				//first time load file
                $this->load->view('login'); //load view login.php
            }

        }

    }
		

    public function logout()
    {
		//logout 
        $this->session->sess_destroy(); //destroy session
        redirect(base_url().'main'); //redirect to controller main
    }
	
}
