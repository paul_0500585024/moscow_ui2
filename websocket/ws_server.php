<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

    // Make sure composer dependencies have been installed
    require __DIR__ . '/Ratchet/vendor/autoload.php';
	require __DIR__ . '/Pawl/vendor/autoload.php';
//	require __DIR__ . '/websocket-php/vendor/autoload.php';
//use WebSocket\Client;
/**
 * chat.php
 * Send any incoming messages to all connected clients (except sender)
 */
class MyChat implements MessageComponentInterface {
    protected $clients;


    public function __construct() {
        $this->clients = new \SplObjectStorage;
		$loop = React\EventLoop\Factory::create();
		$connector = new Ratchet\Client\Connector($loop);
//		$this->data_trader = "halo paul";
//		$data_trader = $this->data_trader;
		$app = function (Ratchet\Client\WebSocket $conn) use ($connector, $loop, &$app) {
			$conn->on('message', function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn, $connector, $loop, &$app) {
//				echo "Received: {$msg}\n";
//				sleep(10);
//				$conn->close();
//				foreach ($this->clients as $client) {
//					$client->send($msg);
//				}
				foreach ($this->clients as $client) {
					$client->send($msg);
				}
//				echo "{$msg}\n";
				$loop->addTimer(1, function () use ($conn, $connector, $loop, $app) {
					$conn->close();
				});
			});

			$conn->on('close', function ($code = null, $reason = null) use ($connector, $loop, $app) {
				echo "Connection closed ({$code} - {$reason})\n";

				//in 3 seconds the app will reconnect
//				$loop->addTimer(3, function () use ($connector, $loop, $app) {
//					$this->connectToServer($connector, $loop, $app);
//				});
			});

			$conn->send($this->msg);
		};
		$this->loop = $loop;
		$this->connector = $connector;
		$this->app = $app;
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
    }
	
	private function connectToServer($connector, $loop, $app)
	{
		$connector('ws://10.48.1.76:8282')
			->then($app, function (\Exception $e) use ($loop) {
				echo "Could not connect: {$e->getMessage()}\n";
				$loop->stop();
			});
	}	

    public function onMessage(ConnectionInterface $from, $msg) {
		$this->msg = $msg;
//		echo $this->msg.'\n';
		$this->connectToServer($this->connector, $this->loop, $this->app);
		$this->loop->run();
/*        foreach ($this->clients as $client) {
			$this->client_ws->send("Hello WebSocket.org!");
			echo $this->client_ws->receive();
//            if ($from != $client) {
//                $client->send($msg);
//            }
        } */
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
}

/*		\Ratchet\Client\connect('wss://echo.websocket.org:443')->then(function($conn) {
				$conn->on('message', function($msg) use ($conn) {
					echo "Received: {$msg}\n";
					$conn->close();
				});

				$conn->send('Hello World!');
			}, function ($e) {
				echo "Could not connect: {$e->getMessage()}\n";
			});*/
/*
$loop = \React\EventLoop\Factory::create();
    $reactConnector = new \React\Socket\Connector($loop, [
        'dns' => '8.8.8.8',
        'timeout' => 10
    ]);
    $connector = new \Ratchet\Client\Connector($loop, $reactConnector);

    $connector('ws://127.0.0.1:9000', ['protocol1', 'subprotocol2'], ['Origin' => 'http://localhost'])
    ->then(function(Ratchet\Client\WebSocket $conn) {
        $conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($conn) {
            echo "Received: {$msg}\n";
            $conn->close();
        });

        $conn->on('close', function($code = null, $reason = null) {
            echo "Connection closed ({$code} - {$reason})\n";
        });

        $conn->send('Hello World!');
    }, function(\Exception $e) use ($loop) {
        echo "Could not connect: {$e->getMessage()}\n";
        $loop->stop();
    });

    $loop->run();			*/


    // Run the server application through the WebSocket protocol on port 8080
    $app = new Ratchet\App('0.0.0.0', 8911, '0.0.0.0');
    $app->route('/chat', new MyChat, array('*'));
    $app->route('/echo', new Ratchet\Server\EchoServer, array('*'));
    $app->run();